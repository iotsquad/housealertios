//
//  CornerRadiusButton.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 6/1/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

class CornerRadiusButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
}

extension CornerRadiusButton {
    
    fileprivate func initialSetup() {
        backgroundColor = ColorPalette.turquoise
        layer.cornerRadius = bounds.height / 2
        layer.masksToBounds = true
        setTitleColor(UIColor.white, for: .normal)
    }
    
}
