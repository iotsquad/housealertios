//
//  AlertTableViewCell.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 5/30/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

class AlertTableViewCell: UITableViewCell {

    var timeAndLocation : String! {
        didSet{
            timeAndLocationLabel.text = timeAndLocation
        }
    }
    
    @IBOutlet fileprivate weak var timeAndLocationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = ColorPalette.cellColor
    }
    
}
