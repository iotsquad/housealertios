//
//  HomeCoordinator.swift
//  HouseAlert
//
//  Created by Juanjo on 5/31/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

protocol HomeCoordinatorDelegate: class {
    func homeCoordinatorDidFinish(homeCoordinator: HomeCoordinator)
}

class HomeCoordinator: Coordinator {
    var tabBarViewController: UITabBarController?
    weak var delegate: HomeCoordinatorDelegate?
    var window: UIWindow
    
    init(window: UIWindow){
        self.window = window
    }
    
    func start() {
        
        /*let homeViewController = HomeViewController()
        let navigationController = UINavigationController(rootViewController: homeViewController)
        homeViewController.coordinatorDelegate = self
        window.rootViewController = navigationController*/
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        tabBarViewController = storyboard.instantiateInitialViewController() as? UITabBarController
        let navigationViewController = tabBarViewController?.viewControllers?[0] as? UINavigationController
        let statisticsViewController = navigationViewController?.viewControllers[0] as? StatisticsViewController
        statisticsViewController?.coordinatorDelegate = self
        window.rootViewController = tabBarViewController
    }
    
    deinit {
        print("\nDEINIT HOME COORDINATOR \n")
    }
    
}

extension HomeCoordinator: HomeViewControllerCoordinatorDelegate {
    
    func homeViewControllerDidFinish(homeViewController: HomeViewController) {
        delegate?.homeCoordinatorDidFinish(homeCoordinator: self)
    }
    
}

extension HomeCoordinator: StatisticsViewControllerCoordinatorDelegate {
    
    func statisticsViewControllerDidFinish(statisticsViewController: StatisticsViewController) {
        delegate?.homeCoordinatorDidFinish(homeCoordinator: self)
    }
    
}
