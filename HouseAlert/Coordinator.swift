//
//  Coordinator.swift
//  HouseAlert
//
//  Created by Juan José Ramírez Calderón on 5/28/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    func start()
}
