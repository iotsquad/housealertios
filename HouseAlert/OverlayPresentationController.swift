//
//  OverlayPresentationController.swift
//  myGigSales
//
//  Created by Carlos H Montenegro on 4/6/17.
//  Copyright © 2017 Independent. All rights reserved.
//

import UIKit

class OverlayPresentationController: UIPresentationController {
    
    var contentHeight : CGFloat!
    
    fileprivate var dimmingView : UIView!
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        dimmingViewSetup()
    }
    
    override func presentationTransitionWillBegin() {
        guard let container = containerView else { return }
        
        container.insertSubview(dimmingView, at: 0)
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dimmingView]|",options: [], metrics: nil, views: ["dimmingView": dimmingView]))
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 1.0
        })
    }

    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0
        })
    }
    
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        guard let thisHeight = contentHeight else { return CGSize.zero }
        
        let ratio : CGFloat = 2.5/3.0
        
        return CGSize(width: parentSize.width * ratio, height: thisHeight)
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let container = containerView else { return CGRect.zero }
        var frame = CGRect.zero
        
        presentedViewController.view.layer.cornerRadius = 10.0
        presentedViewController.view.layer.masksToBounds = true
        
        frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: container.bounds.size)
        
        frame.origin.x = (container.frame.width - frame.size.width) / 2.0
        frame.origin.y = (container.frame.height - frame.size.height) / 2.0
        
        return frame
    }
}

extension OverlayPresentationController {
    
    fileprivate func dimmingViewSetup() {
        dimmingView = UIView()
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        dimmingView.alpha = 0.0
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(OverlayPresentationController.handleTap(recoginzer:)))
        dimmingView.addGestureRecognizer(recognizer)
    }
    
    dynamic func handleTap(recoginzer: UITapGestureRecognizer) {
        presentingViewController.dismiss(animated: true, completion: nil)
    }
    
}
