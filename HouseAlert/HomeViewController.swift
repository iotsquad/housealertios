//
//  HomeViewController.swift
//  HouseAlert
//
//  Created by Juan José Ramírez Calderón on 5/28/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import Charts

protocol HomeViewControllerCoordinatorDelegate: class {
    func homeViewControllerDidFinish(homeViewController: HomeViewController)
}

class HomeViewController: UIViewController {

    weak var coordinatorDelegate: HomeViewControllerCoordinatorDelegate?
    var ref: DatabaseReference = Database.database().reference()
    var chartDataSets = [LineChartDataSet(), LineChartDataSet()]

    let chartView: LineChartView = {
        let chart = LineChartView()
        chart.translatesAutoresizingMaskIntoConstraints = false
        return chart
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupConstraints()
        setupChart()
        
        
    }

    func didTapLogOutButton(){
        try! Auth.auth().signOut()
        GIDSignIn.sharedInstance().signOut()
        coordinatorDelegate?.homeViewControllerDidFinish(homeViewController: self)
    }

    deinit {
        print("\nDEINIT HOME\n")
    }

}

// MARK: - Setup

extension HomeViewController {
    
    func setupView(){
        view.backgroundColor = .white
        title = "HOUSE ALERT"
        print("INIT HOME")
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.done, target: self, action: #selector(didTapLogOutButton))
        view.addSubview(chartView)
    }
    
    func setupConstraints(){
        chartView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        chartView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30).isActive = true
        chartView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 30).isActive = true
        chartView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30).isActive = true
    }
    
    func setupChart(){
        
        ref.child("today_humidity_percentage").observe(.value, with: { (snapshot) in
            let values = snapshot.value as? [Double] ?? []
            
            var dataEntries: [ChartDataEntry] = []
            
            for i in 0 ..< values.count {
                let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                dataEntries.append(dataEntry)
            }
            
            self.chartView.chartDescription?.text = ""
            
            let chartDataSet = LineChartDataSet(values: dataEntries, label: "Humidity %")
            chartDataSet.axisDependency = .left
            chartDataSet.colors = [UIColor.green]
            chartDataSet.circleColors = [UIColor.green]
            chartDataSet.mode = .cubicBezier
            self.chartDataSets[0] = chartDataSet
            
            let chartData = LineChartData(dataSets: self.chartDataSets)
            
            self.chartView.data = chartData
            
            self.chartView.animate(xAxisDuration: 0, yAxisDuration: 2.0)
        })
        
        ref.child("today_temperature_celcius").observe(.value, with: { (snapshot) in
            let values = snapshot.value as? [Double] ?? []
            
            var dataEntries: [ChartDataEntry] = []
            
            for i in 0 ..< values.count {
                let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                dataEntries.append(dataEntry)
            }
            
            self.chartView.chartDescription?.text = ""
            
            let chartDataSet = LineChartDataSet(values: dataEntries, label: "Temperature C°")
            chartDataSet.axisDependency = .left
            self.chartDataSets[1] = chartDataSet
            let chartData = LineChartData(dataSets: self.chartDataSets)
            
            self.chartView.data = chartData
            
            self.chartView.animate(xAxisDuration: 0, yAxisDuration: 2.0)
        })

        let xAxis = chartView.xAxis
        xAxis.labelPosition = .top
        xAxis.drawGridLinesEnabled = false
        
        let yAxis = chartView.leftAxis
        yAxis.drawGridLinesEnabled = false
        
        chartView.rightAxis.enabled = false
        chartView.legend.enabled = true
        
        
        
    }
}
