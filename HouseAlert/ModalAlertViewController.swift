//
//  ModalAlertViewController.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 6/1/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

class ModalAlertViewController: UIViewController {
    
    var intrussionLocation : String! {
        didSet{
            intrussionLocationLabel.text = intrussionLocation
        }
    }
    
    var callAction : (() -> Void)?
    
    @IBOutlet fileprivate weak var intrussionLocationLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapCallButton(_ sender: CornerRadiusButton) {
        guard let thisAction = callAction else { return }
        thisAction()
    }
    
    @IBAction func didTapDismissButton(_ sender: CornerRadiusButton) {
        dismiss(animated: true, completion: nil)
    }

}
