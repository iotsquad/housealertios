//
//  LoginViewController.swift
//  HouseAlert
//
//  Created by Juan José Ramírez Calderón on 5/28/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

protocol AuthenticationViewControllerCoordinatorDelegate: class {
    func authenticationViewControllerDidAuthenticateUser(authenticationViewController: AuthenticationViewController)
    //func didLogOutUser(loginViewController: LoginViewController)
}

class AuthenticationViewController: UIViewController, GIDSignInUIDelegate {

    weak var coordinatorDelegate: AuthenticationViewControllerCoordinatorDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        print("INIT LOGIN")
        setupView()
        setupFirebase()
        setupConstraints()
    }

    func setupFirebase(){
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }

    func setupView(){
        view.backgroundColor = .white
        view.addSubview(googleButton)
        view.addSubview(errorLabel)
    }

    deinit {
        print("\nDEINIT LOGIN\n")
    }

    let googleButton: GIDSignInButton = {
        let btn = GIDSignInButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()

    let errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    func setupConstraints(){
        googleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        googleButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        errorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorLabel.topAnchor.constraint(equalTo: googleButton.bottomAnchor).isActive = true
    }


}

extension AuthenticationViewController: GIDSignInDelegate {

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            print(error.localizedDescription)
            return
        }

        guard let authentication = user.authentication else { return }

        guard user.profile.email.hasSuffix("@tektonlabs.com") else {
            errorLabel.text = "You must log in with a tektonlabs account"
            errorLabel.isHidden = false
            return GIDSignIn.sharedInstance().signOut()
        }

        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)

        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }

            self.coordinatorDelegate?.authenticationViewControllerDidAuthenticateUser(authenticationViewController: self)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!, withError error: Error!) {
        // do something on disconect
    }

}
