//
//  Movement.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 6/1/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import Foundation

class Movement {
    
    var id        : Int
    var value     : String
    var sensor    : String
    var timeStamp : String
    
    init(id: Int, value: String, sensor: String, timeStamp: String) {
        self.id = id
        self.value = value
        self.sensor = sensor
        self.timeStamp = timeStamp
    }
    
    convenience init?(withJson json: [String: Any]) {
        guard let id        = json["id"]         as? Int,
              let value     = json["value"]      as? String,
              let sensor    = json["sensor"]     as? String,
              let timeStamp = json["created_at"] as? String
        else { return nil }
        self.init(id: id, value: value, sensor: sensor, timeStamp: timeStamp)
    }
    
}
