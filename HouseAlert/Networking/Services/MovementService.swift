//
//  MovementService.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 6/1/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import Foundation

class MovementService {
    
    var requestBuilder = RequestBuilder()
    
    func getMovementList(completion: @escaping (_ movements: [Movement]?, _ error: Error?) -> Void) {
        let request = requestBuilder.makeRequest(with: [:], path: "movements.json", token: "", method: .get)
        let networkManager = NetworkManager()
        let dataTask = networkManager.dataTask(with: request) { (result) in
            
            switch result {
                case .success(let result):
                    var movements = [Movement]()
                    let arr = result as! [Any]
                    
                    for object in arr {
                        let optionDictionary = object as! [String: Any]
                        let option = Movement(withJson: optionDictionary)!
                        movements.append(option)
                    }
                    completion(movements, nil)
                case .error(let error):
                    completion(nil, error)
                }
            
        }
        dataTask.resume()
    }
    
}
