//
//  LineChartFormatter.swift
//  HouseAlert
//
//  Created by Juanjo on 6/1/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit
import Charts

@objc(LineChartFormatter)
public class LineChartFormatter: NSObject, IAxisValueFormatter{

    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(Int(value))H"
    }
}
