//
//  ColorPalette.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 5/30/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import Foundation
import UIKit

struct ColorPalette {
    static let appColor = UIColor(red: 41/255, green: 187/255, blue: 156/255, alpha: 1)
    static let cellColor = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)
    static let turquoise = UIColor(red: 78/255, green: 193/255, blue: 167/255, alpha: 1)
    static let skyBlue = UIColor(red: 144/255, green: 234/255, blue: 254/255, alpha: 1)
}
