//
//  OverlayPresentationTransitioningDelegate.swift
//  myGigSales
//
//  Created by Carlos H Montenegro on 4/6/17.
//  Copyright © 2017 Independent. All rights reserved.
//

import UIKit

class OverlayPresentationTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    var contentHeight : CGFloat = 400
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = OverlayPresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.contentHeight = contentHeight
        return presentationController
    }
    
}
