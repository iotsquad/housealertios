//
//  BlackPortraitNavigationController.swift
//  InFlixTesting
//
//  Created by Carlos H Montenegro on 1/20/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import UIKit

@IBDesignable
class BlackPortraitNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension BlackPortraitNavigationController {
    
    fileprivate func initialSetup() {
        navigationBar.barTintColor = ColorPalette.appColor
        UIApplication.shared.statusBarStyle = .lightContent
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        tabBarController?.tabBar.tintColor = ColorPalette.appColor
    }
    
    override var shouldAutorotate: Bool {
        get{
            return false
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get{
            return .portrait
        }
    }
    
}
