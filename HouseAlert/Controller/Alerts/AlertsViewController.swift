//
//  AlertsViewController.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 5/30/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

class AlertsViewController: UIViewController {

    fileprivate var movementList = [Movement]()
    
    @IBOutlet fileprivate weak var alertsTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        loadAlerts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

}

extension AlertsViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movementList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = alertsTableView.dequeueReusableCell(withIdentifier: "movementCell", for: indexPath) as! AlertTableViewCell
        let item = movementList[indexPath.row]
        
        let sensorName = item.sensor
        let movementTime = item.timeStamp.dateFromISO8601!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let time = dateFormatter.string(from: movementTime)
        
        cell.timeAndLocation = "\(time) - \(item.sensor)"
        
        return cell
    }
}

extension AlertsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension AlertsViewController {
    
    fileprivate func initialSetup() {
        tableViewSetup()
        loadAlerts()
    }
    
    fileprivate func tableViewSetup() {
        alertsTableView.delegate = self
        alertsTableView.dataSource = self
        alertsTableView.estimatedRowHeight = 200
        alertsTableView.register(UINib(nibName: "AlertTableViewCell", bundle: nil) , forCellReuseIdentifier: "movementCell")
    }
    
    fileprivate func loadAlerts() {
        LoadingHUD.sharedInstance.show()
        MovementService().getMovementList { (movementList, error) in
            OperationQueue.main.addOperation {
                if let failure = error {
                    LoadingHUD.sharedInstance.hide()
                    print(failure)
                } else {
                    guard let movements = movementList else { return }
                    self.movementList = movements
                    self.alertsTableView.reloadData()
                    LoadingHUD.sharedInstance.hide()
                }
            }
        }
        
    }
    
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension String {
    var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
    }
}

