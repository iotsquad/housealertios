//
//  StatisticsViewController.swift
//  HouseAlert
//
//  Created by Carlos H Montenegro on 5/30/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import Charts

protocol StatisticsViewControllerCoordinatorDelegate: class {
    func statisticsViewControllerDidFinish(statisticsViewController: StatisticsViewController)
}

class StatisticsViewController: UIViewController {

    @IBOutlet fileprivate weak var temperatureView: UIView!
    @IBOutlet fileprivate weak var humidityView: UIView!
    @IBOutlet fileprivate weak var statisticsView: UIView!
    @IBOutlet fileprivate weak var lineChartView: LineChartView!
    @IBOutlet fileprivate weak var celsiusTemperatureLabel: UILabel!
    @IBOutlet fileprivate weak var fahrenheitTemperatureLabel: UILabel!
    @IBOutlet fileprivate weak var humidityLabel: UILabel!
    
    var ref: DatabaseReference = Database.database().reference()
    var chartDataSets = [LineChartDataSet(), LineChartDataSet()]

    weak var coordinatorDelegate: StatisticsViewControllerCoordinatorDelegate?
    
    fileprivate var overlayTransitioningDelegate = OverlayPresentationTransitioningDelegate()
    
    fileprivate var emergencyNumber = "+911"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setupChart()
        setupLabels()
    }
    
    deinit {
        print("deinit statistics")
    }

}

// MARK: - Actions

extension StatisticsViewController {
    
    func didTapLogOutButton(){
        try! Auth.auth().signOut()
        GIDSignIn.sharedInstance().signOut()
        coordinatorDelegate?.statisticsViewControllerDidFinish(statisticsViewController: self)
    }
    
    //MARK: - Testing Alert Method (Delete when not necessary)
    
    func didTapModalTestButton() {
        let storyboard = UIStoryboard(name: "CustomAlerts", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "modalAlertViewController") as! ModalAlertViewController
        viewController.transitioningDelegate = overlayTransitioningDelegate
        viewController.modalPresentationStyle = .custom
        
        viewController.callAction = { [weak self] in
            self?.dismiss(animated: true, completion: {
                print("CALLING 911")
            })
        }
        
        present(viewController, animated: true, completion: nil)
    }
    
}

// MARK: - Visual Setup

extension StatisticsViewController {
    
    fileprivate func initialSetup() {
        setCorneredViews()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "exitIcon"), style: .plain, target: self, action: #selector(didTapLogOutButton))
        
        //navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "alertsIcon"), style: .plain, target: self, action: #selector(didTapModalTestButton))
        //navigationItem.rightBarButtonItem?.tintColor = .white
        
        navigationItem.leftBarButtonItem?.tintColor = .white
        
    }
    
    fileprivate func setCorneredViews() {
        temperatureView.layer.cornerRadius = 5
        humidityView.layer.cornerRadius = 5
        statisticsView.layer.cornerRadius = 5
        temperatureView.layer.masksToBounds = true
        humidityView.layer.masksToBounds = true
        statisticsView.layer.masksToBounds = true
    }
    
    func plotChart(from values: [Double], labelText: String="", color: UIColor, index: Int){
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0 ..< values.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = LineChartDataSet(values: dataEntries, label: labelText)
        chartDataSet.axisDependency = .left
        chartDataSet.colors = [color]
        chartDataSet.circleColors = [color]
        chartDataSet.drawValuesEnabled = false
        chartDataSet.mode = .cubicBezier
        chartDataSet.drawFilledEnabled = true
        chartDataSet.fillColor = color
        chartDataSet.lineWidth = 4
        self.chartDataSets[index] = chartDataSet
        
        let chartData = LineChartData(dataSets: self.chartDataSets)
        
        self.lineChartView.data = chartData
        
        self.lineChartView.animate(xAxisDuration: 0, yAxisDuration: 2.0)
    }
    
    func setupChart(){
        
        ref.child("today_humidity_percentage").observe(.value, with: { (snapshot) in
            let values = snapshot.value as? [Double] ?? []
            self.plotChart(from: values, labelText: "Humidity %", color: ColorPalette.skyBlue, index: 0)
        })
        
        ref.child("today_temperature_celcius").observe(.value, with: { (snapshot) in
            let values = snapshot.value as? [Double] ?? []
            self.plotChart(from: values, labelText: "Temperature C°", color: ColorPalette.turquoise, index: 1)
        })
        
        let format = LineChartFormatter()
        
        let xAxis = lineChartView.xAxis
        xAxis.labelPosition = .top
        xAxis.drawGridLinesEnabled = false
        xAxis.valueFormatter = format
        
        let yAxis = lineChartView.leftAxis
        yAxis.drawGridLinesEnabled = true
        
        lineChartView.rightAxis.enabled = false
        lineChartView.legend.enabled = true
        lineChartView.chartDescription?.enabled = false
        
    }
    
    func setupLabels(){
        ref.child("last_temperature_celcius").observe(.value, with: { (snapshot) in
            guard let value = snapshot.value as? Double else { return }
            self.celsiusTemperatureLabel.text = "\(Int(value))° C"
            self.fahrenheitTemperatureLabel.text = "\(Int(value * 1.8 + 32))° F"
        })
        
        ref.child("last_humidity_percentage").observe(.value, with: { (snapshot) in
            guard let value = snapshot.value as? Double else { return }
            self.humidityLabel.text = "\(value)%"
            
        })
    }
    
    
}
