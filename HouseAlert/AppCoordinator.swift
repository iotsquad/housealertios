//
//  AppCoordinator.swift
//  HouseAlert
//
//  Created by Juan José Ramírez Calderón on 5/28/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit
import Firebase

class AppCoordinator: Coordinator {
    
    enum Key { case authentication, home, none }

    var coordinators = [Key: Coordinator]()
    var window: UIWindow

    init(window: UIWindow){
        self.window = window
    }

    func start() {
        userIsLoggedIn ?  showHome() : showAuthentication()
    }

    var userIsLoggedIn: Bool {
        return (Auth.auth().currentUser != nil)
    }

    func showAuthentication(){
        let authenticationCoordinator = AuthenticationCoordinator(window: window)
        coordinators[Key.authentication] = authenticationCoordinator
        authenticationCoordinator.delegate = self
        authenticationCoordinator.start()
    }

    func showHome(){
        let homeCoordinator = HomeCoordinator(window: window)
        coordinators[Key.home] = homeCoordinator
        homeCoordinator.delegate = self
        homeCoordinator.start()
    }
    
}

extension AppCoordinator: AuthenticationCoordinatorDelegate {
    
    func authenticationCoordinatorDidFinish(authenticationCoordinator: AuthenticationCoordinator) {
        // only reason for ending is auth success
        coordinators[Key.authentication] = nil
        showHome()
    }
    
}

extension AppCoordinator: HomeCoordinatorDelegate {
    
    func homeCoordinatorDidFinish(homeCoordinator: HomeCoordinator) {
        // only reason for ending is logout
        coordinators[Key.home] = nil
        showAuthentication()
    }
    
}
