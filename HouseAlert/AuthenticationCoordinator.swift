//
//  AuthenticationCoordinator.swift
//  HouseAlert
//
//  Created by Juanjo on 5/31/17.
//  Copyright © 2017 Tekton Labs. All rights reserved.
//

import UIKit

protocol AuthenticationCoordinatorDelegate: class {
    func authenticationCoordinatorDidFinish(authenticationCoordinator: AuthenticationCoordinator)
}

class AuthenticationCoordinator: Coordinator {
    
    var coordinators =  [String : Coordinator]()
    weak var delegate: AuthenticationCoordinatorDelegate?
    var window: UIWindow
    var authenticationViewController: AuthenticationViewController?
    
    init(window: UIWindow){
        self.window = window
    }
    
    func start() {
        authenticationViewController = AuthenticationViewController()
        authenticationViewController?.coordinatorDelegate = self
        window.rootViewController = authenticationViewController
    }
    
}

extension AuthenticationCoordinator: AuthenticationViewControllerCoordinatorDelegate {
    
    func authenticationViewControllerDidAuthenticateUser(authenticationViewController: AuthenticationViewController) {
        delegate?.authenticationCoordinatorDidFinish(authenticationCoordinator: self)
    }
    
}

